package com.examen;

import java.util.Scanner;

public class Palindromo implements PalindromoInterfaz {

	@Override
	public void identificarPalindromo() {

		String original, reverse = "";
		Scanner sc = new Scanner(System.in);
		
		System.out.println("Ingrese una palabra para verificar si es un Pal�ndromo: ");
	    original = sc.nextLine();
	    
	    int length = original.length();
	    
	    for ( int i = length - 1; i >= 0; i-- )
	    	reverse = reverse + original.charAt(i);
	 
	    if (original.equals(reverse))
	    	System.out.println("S�, es un Pal�ndromo.");
	    else
	    	System.out.println("No, no es un Pal�ndromo.");
		
	}
	
}
